%---------------------------------------------------------------------------------------------------
%
%	Edited by: Copyright (c) 2016 Jakub Kúdela
%   Based on: Copyright (c) 2015 Jan Küster
%
%	The MIT License (MIT)
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%
%---------------------------------------------------------------------------------------------------


%===================================================================================================
%	DOCUMENT DEFINITION
%===================================================================================================

%we use article class because we want to fully customize the page and dont use a cv template
\documentclass[10pt,a4paper]{article}	

%---------------------------------------------------------------------------------------------------
%	ENCODING
%---------------------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}		

%---------------------------------------------------------------------------------------------------
%	LOGIC
%---------------------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xifthen}

%---------------------------------------------------------------------------------------------------
%	FONT
%---------------------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto}
\usepackage[hidelinks]{hyperref}

% set font default
\renewcommand*\familydefault{\sfdefault} 	
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}		

%---------------------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%---------------------------------------------------------------------------------------------------

%debug page outer frames
%\usepackage{showframe}			

%define page styles using geometry
\usepackage[a4paper]{geometry}		

% for example, change the margins to 2 inches all round
\geometry{top=1.55cm, bottom=0cm, left=1.25cm, right=1.25cm} 	

%use customized header
\usepackage{fancyhdr}		
\pagestyle{fancy}

%less space between header and content
\setlength{\headheight}{-8pt}		

%customize entries left, center and right
\lhead{}
\chead{\scriptsize{
  Jonathan Shahen $\cdot$ 
  Computer Engineering $\cdot$ 
  Canada $\cdot$ 
  \textcolor{sectcol}{\textbf{\href
  {mailto:jonathan.shahen@gmail.com}
  {jonathan.shahen@gmail.com}}}
}}
\rhead{}

%indentation is zero
\setlength{\parindent}{0mm}

%---------------------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%--------------------------------------------------------------------------------------------------- 

%for layouting tables
\usepackage{multicol}			
\usepackage{multirow}

%extended aligning of tabular cells
\usepackage{array}
\newcolumntype{x}[1]{>{\raggedleft\hspace{0pt}}p{#1}}

%---------------------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%--------------------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}

%for floating figures
\usepackage{wrapfig}
\usepackage{float}
%\floatstyle{boxed} 
%\restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}

%---------------------------------------------------------------------------------------------------
%	Color DEFINITIONS
%--------------------------------------------------------------------------------------------------- 

\usepackage{color}

%accent color
\definecolor{sectcol}{RGB}{90,90,120}

%dark background color
\definecolor{bgcol}{RGB}{110,110,110}

%light background / accent color
\definecolor{softcol}{RGB}{225,225,225}

%===================================================================================================
%	DEFINITIONS
%===================================================================================================

%---------------------------------------------------------------------------------------------------
% 	HEADER
%---------------------------------------------------------------------------------------------------

% remove top header line
\renewcommand{\headrulewidth}{0pt} 

%remove botttom header line
\renewcommand{\footrulewidth}{0pt}	  	

%remove pagenum
\renewcommand{\thepage}{}	

%remove section num		
\renewcommand{\thesection}{}			

%---------------------------------------------------------------------------------------------------
% 	ARROW GRAPHICS in Tikz
%---------------------------------------------------------------------------------------------------

% a six pointed arrow poiting to the left
\newcommand{\tzlarrow}{(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;}	

% include the left arrow into a tikz picture
% param1: fill color
%
\newcommand{\larrow}[1]{
  \begin{tikzpicture}[scale=0.58]
    \filldraw[fill=#1!100,draw=#1!100!black]  \tzlarrow
  \end{tikzpicture}
}

% a six pointed arrow poiting to the right
\newcommand{\tzrarrow}{ (0,0.2) -- (0.1,0) -- (0.3,0) -- (0.2,0.2) -- (0.3,0.4) -- (0.1,0.4) -- cycle;}

% include the right arrow into a tikz picture
% param1: fill color
%
\newcommand{\rarrow}{
  \begin{tikzpicture}[scale=0.7]
    \filldraw[fill=sectcol!100,draw=sectcol!100!black] \tzrarrow
  \end{tikzpicture}
}
%---------------------------------------------------------------------------------------------------
%	custom sections
%---------------------------------------------------------------------------------------------------

% create a coloured box with arrow and title as cv section headline
% param 1: section title
%
\newcommand{\cvsection}[1]{
  \vspace{10pt}
  \colorbox{sectcol}{\mystrut \makebox[1\linewidth][l]{
    \larrow{bgcol} \hspace{-8pt} \larrow{bgcol} \hspace{-8pt} 
    \larrow{bgcol}\textcolor{white}{\textbf{#1}}\hspace{4pt}
  }}\\
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\metasection}[2]{
  \begin{tabular*}{1\textwidth}{p{2.4cm} p{15cm}}
    \larrow{bgcol} \normalsize{\textcolor{sectcol}{#1}}&#2\\[2pt]
  \end{tabular*}
}
\newcommand{\metasectionb}[2]{
  \begin{tabular*}{1\textwidth}{p{2.4cm} p{14.5cm}}
    \larrow{bgcol} \normalsize{\textcolor{sectcol}{#1}}&#2\\[10pt]
  \end{tabular*}
}

%---------------------------------------------------------------------------------------------------
%	 CV EVENT
%---------------------------------------------------------------------------------------------------

% creates a stretched box as cv entry headline followed by two paragraphs
% param 1:	event time i.e. 2014 or 2011-2014 etc.
% param 2:	event name (what did you do?)
% param 3:	institution (where did you work / study)
%
\newcommand{\cvevent}[3]{
  \begin{tabular*}{1\textwidth}{p{2.5cm} p{10.5cm} x {4.0cm}}
    \textcolor{bgcol}{#1} & \textbf{#2} & \vspace{2.5pt}\textcolor{sectcol}{#3}
  \end{tabular*}
  \vspace{-10pt}
  \textcolor{softcol}{\hrule}
  \vspace{5pt}
}
\newcommand{\cveventl}[3]{
  \begin{tabular*}{1\textwidth}{p{2.5cm} p{6.5cm} x {8.0cm}}
    \textcolor{bgcol}{#1} & \textbf{#2} & \vspace{2.5pt}\textcolor{sectcol}{#3}
  \end{tabular*}
  \vspace{-10pt}
  \textcolor{softcol}{\hrule}
  \vspace{10pt}
}

% creates a stretched box as cv entry detail 
% param 1:	information describing the event
%
\newcommand{\cvdetail}[1]{
  \begin{tabular*}{1\textwidth}{p{2.5cm} p{14.5cm}}
    & \larrow{bgcol}  #1\\ [3pt]
  \end{tabular*}
}

%---------------------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%---------------------------------------------------------------------------------------------------
\newcommand{\mystrut}{\rule[-.3\baselineskip]{0pt}{\baselineskip}}

%===================================================================================================
%	DOCUMENT CONTENT
%===================================================================================================
\title{resume}
\begin{document}

% use our custom fancy header definitions
\pagestyle{fancy}	

%---------------------------------------------------------------------------------------------------
%	TITLE HEADLINE
%---------------------------------------------------------------------------------------------------

\vspace{-20pt}

% use this for single words, e.g. CV or RESUME etc.
\hspace{-0.25\linewidth}\colorbox{bgcol}{
  \makebox[1.5\linewidth][c]{
    \HUGE{\textcolor{white}{\textsc{Jonathan Shahen}}} 
    \textcolor{sectcol}{\rule[-1mm]{1mm}{0.9cm}} 
    \HUGE{\textcolor{white}{\textsc{Resume}}}
  }
}

%---------------------------------------------------------------------------------------------------
%	HEADER IMAGE
%---------------------------------------------------------------------------------------------------

\begin{figure}[H]
\begin{flushright}
  \includegraphics[width=0.12\linewidth]{../headshots/IMG_20190818_161702_DRO}
\end{flushright}
\end{figure}

%---------------------------------------------------------------------------------------------------
%	META SECTION
%---------------------------------------------------------------------------------------------------

\vspace{-95pt}

\metasection{Status:}{Finishing PhD. in Computer Engineering}
\metasection{Skills:}{Java, C/C++, Python, JavaScript, Node Js, Bash, SQL, Tensorflow, Solidity} 
\metasection{Interests:}{Computer Security, Machine Learning, Research and Development}
\metasection{Activities:}{Running, Hiking, Rock Climbing, Reading, Music}

%---------------------------------------------------------------------------------------------------
%	SUMMARAY (optional)
%---------------------------------------------------------------------------------------------------

\cvsection{Summary}

An avid problem solver, who enjoys problems that do not have a known solution.
Through research has grown to appreciate that home grown solutions might not always be the best first approach, but understanding the problem can often lead to a solution that was created for another problem.
Home brewed solutions are very useful for creating solutions that optimize selected criteria, but often require a long development period.

%===================================================================================================
%	CV SECTIONS AND EVENTS (MAIN CONTENT)
%===================================================================================================

%---------------------------------------------------------------------------------------------------
%	EDUCATION SECTION
%---------------------------------------------------------------------------------------------------
\cvsection{Education}

\cvevent{2016 - 2020}{PhD Degree, Computer Engineering}{University of Waterloo}
\cvdetail{\emph{Thesis:} Solving Computer Security Problems by Reducing to Known Problems with Mature Off-the-Shelf Solvers.}
\cvdetail{\emph{Problem Spaces:} Access Control, Cloud Security, and Logic Locking}

\cvevent{2014 - 2016}{Master's Degree, Computer Engineering}{University of Waterloo}
\cvdetail{\emph{Thesis:} Automated Safety Analysis of Administrative Temporal Role-Based Access Control (ATRBAC) Policies using Mohawk+T}
\cvdetail{\emph{Courses:} Algorithm Design and Analysis, Tools of Intelligent Systems Design, Computer Network Security, Software Reliability}

\cvevent{2009 - 2014}{Bachelor's Degree, Computer Engineering}{University of Waterloo}
\cvdetail{\emph{Courses:} Compilers, Database Systems, Computer Networks, Distributed Computing, Computer Security, Cooperative and Adaptive Algorithms, Fundamentals of Computational Intelligence}

%---------------------------------------------------------------------------------------------------
%	Papers
%---------------------------------------------------------------------------------------------------
\cvsection{Publications}
\metasectionb{TDSC'19}{Cree: Performant Tool for Safety Analysis of Administrative Temporal Role-Based Access Control (ATRBAC) Policies}
\metasectionb{TDSC'18}{The Overhead from Combating Side-Channels in Cloud Systems using VM-Scheduling}
\metasectionb{SACMET'15}{Mohawk+T: Efficient Analysis of Administrative Temporal Based Access Control (ATRBAC) Policies}


%---------------------------------------------------------------------------------------------------
%	EXPERIENCE
%---------------------------------------------------------------------------------------------------
\cvsection{Experience}

\cvevent{Sep - Dec 2013}{Machine Learning}{Thalmic Labs Inc.}
\cvdetail{Machine Learning research and development for the Myo gesture controlled device. Focus on Support Vector Machines.}

\cvevent{Sep - Dec 2012}{Software Engineer}{DemonWare Inc.}
\cvdetail{Engineering design for high performance and big data for Call Of Duty Online marketplace.}

%------------------------------------------------

\cveventl{Jan - Apr 2012}{Machine Learning Researcher}{Department of National Defence}
\cvdetail{Machine Learning research applied to Multi-Criteria Decision Analysis (MCDA) problems.}

%------------------------------------------------

\cvevent{May - Aug 2011}{Advance Compression Researcher}{RIM}
\cvdetail{Optimized JPEG Compression Algorithm for small circuit design.}

%---------------------------------------------------------------------------------------------------
%	ARTIFICIAL FOOTER (fancy footer cannot exceed linewidth) 
%---------------------------------------------------------------------------------------------------

\null
\vspace*{\fill}

\hspace{-0.25\linewidth}\colorbox{bgcol}{
  \makebox[1.5\linewidth][c]{
    \mystrut \small 
    \textcolor{white}{
      \href{https://www.linkedin.com/in/jonathan-shahen/}
      {linkedin.com/in/jonathan-shahen}
    } $\cdot$ 
    \textcolor{white}{
      \href{https://bitbucket.org/jshahen/}
      {bitbucket.org/jshahen}
    }  $\cdot$ 
    \textcolor{white}{
    \href{https://git.uwaterloo.ca/jmshahen/}
    {git.uwaterloo.ca/jmshahen}
    }
  }
}

%===================================================================================================
%	DOCUMENT END
%===================================================================================================
\end{document}
